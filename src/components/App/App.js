import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Navigation from '../Navigation/Navigation';
import * as routes from '../../constants/routes';

import Landing from '../Landing';
import Login from '../Login';
import Signup from '../Signup';
import MoreInfo from '../MoreInfo';
import withAuth from '../withAuth';
import Home from '../Home';
import Stats from '../Stats';
import HistoryC from '../HistoryC';
class App extends Component {
  
    render() {
      return(
        
        <Router>

          <div>
            <Navigation/>

            <Route
              exact path={routes.LANDING}
              component={Landing}
            />
            <Route
              exact path={routes.HOME}
              component={Home}
            />
            <Route
              exact path={routes.HISTORY}
              component={HistoryC}
            />
            <Route
              exact path={routes.STATS}
              component={Stats}
            />
            <Route
              exact path={routes.SIGN_IN}
              component={Login}
            />
            <Route
              exact path={routes.SIGN_UP}
              component={Signup}
            />
            <Route
              exact path={routes.MOREINFO}
              component={MoreInfo}
            />
          </div>

        </Router>
      )
    }
}

export default withAuth(App)
