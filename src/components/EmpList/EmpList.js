import React, { Component } from 'react';
import './EmpList.css';
import { Button } from 'react-bootstrap';
class EmpList extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this.clickHandler = this.clickHandler.bind(this)
    }

    clickHandler() {
        //console.log(this.props.data.timestamp)
        this.props.funcCall(this.props.data)
    }

    render() {
        return (
            <div>
                <div className='box'>
                    <div>
                        <Button onClick={this.clickHandler}>{this.props.data.user.data.name}</Button>
                    </div>
                    
                </div>
            </div>
        )
    }
}

export default EmpList;
