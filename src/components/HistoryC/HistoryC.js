import React, { Component } from 'react';
import { userRef } from '../../firebase/firebase';
import AuthUserContext from '../AuthUserContext';
import moment from 'moment';
import HistoryList from '../HistoryList';
class HistoryC extends Component {
    constructor(props) {
        super(props);
        this.state = {
            au: null,
            user: null, 
            isLoading: true,
            ip: null,
            session: null,
            sessionLoad: false
        }

        this.renderSession = this.renderSession.bind(this);
        this.callHandler = this.callHandler.bind(this);
    }

    componentDidMount() {
        this.renderSession()
    }

   
    renderSession() {
        if(this.props.user.Sessions){
            if(this.props.user.Sessions.History) {
                console.log(this.props.user.Sessions.History)
                let obj = this.props.user.Sessions.History
                let arr = [];
                let i = 0;
                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        let temp = obj[key]
                        temp.key = key
                        arr[i] = temp
                        
                        i++
                    }
                }
                this.setState({session: arr, sessionLoad: true})
            }
            else {
                this.setState({session : null, sessionLoad: true})    
            }
            
        }
        
    }

    renderList() {

    }

    callHandler(data) {
        console.log(data.key)
        
        userRef.child(this.props.user.uid).child('Sessions').child('Ongoing').child(data.key).remove()
        .then(() => {
            console.log("Success")
            userRef.child(this.props.user.uid).child('Sessions').child('History').child(data.key).update({...data, timestampEnd: moment().format()})
            .then(() => {
                console.log("Entered to History")
            })
            .catch((error) => console.log(error))
        })
        .catch((error) => console.log(error));
    }

    render() {
        const {user} = this.props
        return(
            
            user ?
                
                <div className='row myWindow'>
                    <div className='col-lg-12 myFont '>       
                            <div>
                            <h2>History </h2>
                                {
                                    this.state.session && this.state.sessionLoad ?
                                    this.state.session.map((data) => {
                                        return (
                                            <HistoryList data={data} funcCall={this.callHandler}/>
                                        )
                                    })
                                    :
                                    <div><h2>No History</h2></div>
                                }
                            
                            
                        </div>
                    </div>
                </div>
                
            :
            <div><h2>Loading</h2></div>
        

        )
    }

    
}

export default props => (
    <AuthUserContext.Consumer>
        {user=>{
            return (
                user ? <HistoryC {...props} user={user} />
                : <div/>
            )
        }}
    </AuthUserContext.Consumer>        
)
