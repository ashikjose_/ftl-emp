import React, { Component } from 'react';
import './HistoryList.css';
import moment from 'moment';

class HistoryList extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this.clickHandler = this.clickHandler.bind(this)
    }

    clickHandler() {
        //console.log(this.props.data.timestamp)
        this.props.funcCall(this.props.data)
    }

    render() {
        return (
            <div>
                <div className='box'>
                    
                    <div>
                        <h2>{this.props.data.ip}</h2>
                        <h6>{moment.duration(moment(this.props.data.timestampEnd).diff(moment(this.props.data.timestamp),'minutes'), "minutes").format("h [hrs], m [min]")}</h6>
                    </div>
                    
                </div>
            </div>
        )
    }
}

export default HistoryList;
