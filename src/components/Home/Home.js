import React, { Component } from 'react';
import './Home.css';
import { userRef } from '../../firebase/firebase';
import AuthUserContext from '../AuthUserContext';
import { Button } from 'react-bootstrap';
import moment from 'moment';
import publicIP from 'react-native-public-ip';
import List from '../List';
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            au: null,
            user: null, 
            isLoading: true,
            ip: null,
            session: null,
            sessionLoad: false
        }
        this.startSession = this.startSession.bind(this);
        this.renderSession = this.renderSession.bind(this);
        this.callHandler = this.callHandler.bind(this);
    }

    componentDidMount() {
        this.renderSession()
    }

    startSession() {
 
        publicIP()
            .then(ip => {
                //console.log(ip);
                userRef.child(this.props.user.uid).child("Sessions").child("Ongoing").push({flag: true, timestamp: moment().format(), ip: ip}).then(()=>{
                    
                    console.log("Hey Just Added")
                })
                .catch((error)=>{
                    console.log(error)
                })
            })
            .catch(error => {
                console.log(error);
                // 'Unable to get IP address.'
        });

        
    }
    
    renderSession() {
        if(this.props.user.Sessions){
            if(this.props.user.Sessions.Ongoing) {
                console.log(this.props.user.Sessions.Ongoing)
                let obj = this.props.user.Sessions.Ongoing
                let arr = [];
                let i = 0;
                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        let temp = obj[key]
                        temp.key = key
                        arr[i] = temp
                        
                        i++
                    }
                }
                this.setState({session: arr, sessionLoad: true})
            }
            else {
                this.setState({session : null, sessionLoad: true})    
            }
            
        }
        
    }

    renderList() {

    }

    callHandler(data) {
        console.log(data.key)
        
        userRef.child(this.props.user.uid).child('Sessions').child('Ongoing').child(data.key).remove()
        .then(() => {
            console.log("Success")
            userRef.child(this.props.user.uid).child('Sessions').child('History').child(data.key).update({...data, timestampEnd: moment().format()})
            .then(() => {
                console.log("Entered to History")
            })
            .catch((error) => console.log(error))
        })
        .catch((error) => console.log(error));
    }

    render() {
        const {user} = this.props
        return(
            
            user ?
                
                <div className='row myWindow'>
                    <div className='col-lg-12 myFont '>       
                            <div>
                            <h2>Home </h2>
                            <h4> Hi, {user.name}</h4>
                             
                            
                                {
                                    this.state.sessionLoad ?
                                    this.state.session.map((data) => {
                                        return (
                                            <List data={data} funcCall={this.callHandler}/>
                                        )
                                    })
                                    :
                                    <div><h2>Loading</h2></div>
                                }
                            
                            <Button bsStyle="primary" bsSize="large" onClick={this.startSession}>
                                Start New Session
                            </Button>
                        </div>
                    </div>
                </div>
                
            :
            <div><h2>Loading</h2></div>
        

        )
    }

    
}

export default props => (
    <AuthUserContext.Consumer>
        {user=>{
            return (
                user ? <Home {...props} user={user} />
                : <div/>
            )
        }}
    </AuthUserContext.Consumer>        
)
