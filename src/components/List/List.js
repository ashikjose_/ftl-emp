import React, { Component } from 'react';
import './List.css';
import moment from 'moment';
import {Button} from 'react-bootstrap';

class List extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this.clickHandler = this.clickHandler.bind(this)
    }

    clickHandler() {
        //console.log(this.props.data.timestamp)
        this.props.funcCall(this.props.data)
    }

    render() {
        return (
            <div>
                <div className='box'>
                    <div className='button'>
                        <Button onClick={this.clickHandler}>End Session</Button>
                    </div>
                    <div>
                        <h2>{this.props.data.ip}</h2>
                        <h6>{moment(this.props.data.timestamp).fromNow()}</h6>
                    </div>
                    
                </div>
            </div>
        )
    }
}

export default List;
