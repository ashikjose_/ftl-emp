import React, { Component } from 'react';
import './Login.css';
import * as validate from '../../validate';
import { auth } from '../../firebase/firebase';
import { withRouter } from 'react-router-dom';
import * as routes from '../../constants/routes';
import { FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap'

class Login extends Component {
    constructor(props) {
        super(props)
        this.onSubmit = this.onSubmit.bind(this);
        this.validateState = this.validateState.bind(this);
        this.renderFunc = this.renderFunc.bind(this);
        this.state = {
            email: '',
            pOne: '',
            errorflag: false,
            error: {},
        }
    }

    validateState() {
        let flag = false
        let error = {}
        this.setState({error: null})

        if(!validate.validateEmail(this.state.email)){
            //console.log("Please enter a valid Email Id")
            flag = true
            error.email = "Please enter a valid Email Id"
        }
        else {
            error.email = ""
        }
        
        if(flag) {
            this.setState({errorflag: flag,error: error})
        }
        
    }

    onSubmit(event) {
        this.validateState()
        const {
            history,
        } = this.props;

        if(this.state.errorflag === false){
            auth.signInWithEmailAndPassword(this.state.email, this.state.pOne)
            .then((resp) => {
                console.log(resp)
                if(this.state.email==='admin@admin.com'){
                    console.log('Hello from admin')
                    history.push({
                        pathname: routes.STATS,
                        //state: { uid: resp.user.uid }  
                    })
                }
                else {
                    history.push({
                        pathname: routes.HOME,
                        //state: { uid: resp.user.uid }  
                    })
                }
                
            })
            .catch((error) => console.log(error));
        }
        event.preventDefault();
    }

    renderFunc() {
        
        if(this.state.errorflag && this.state.error !=null){
            if(this.state.error.username){
                return(
                    <p>{this.state.error.username}</p>
                )
            }
        }
        else {
            return (
                <p>hello</p>
            )
        }
    }

    render() {
        return (
            <div className='myFont bodyDiv row'>
                <div className='myAlign col-lg-1 col-centered'>


                    <form className='form'>
                        <FormGroup
                        controlId="formBasicText"
                        
                        //validationState={this.getValidationState()}
                        >
                            <ControlLabel className='myTitle'>Sign in</ControlLabel>
                            <br/>
                            <br/>
                        
                            <FormControl
                                type="email"
                                value={this.state.email}
                                placeholder="Enter email"
                                onChange={event => this.setState({email: event.target.value})}
                            />
                            <br/>
                            <FormControl
                                type="password"
                                value={this.state.pOne}
                                placeholder="Enter password"
                                onChange={event => this.setState({pOne: event.target.value})}
                            />
                            <FormControl.Feedback />
                            <br/>
                            <Button type="submit" onClick={this.onSubmit}>Submit</Button>
                        </FormGroup>
                    </form>
                    {
                        this.renderFunc()
                    }
                </div>
                
            </div>
        )
    }
}

export default withRouter(Login);