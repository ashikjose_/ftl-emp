import React, { Component } from 'react';
import './MoreInfo.css';
import * as validate from '../../validate';
import { userRef } from '../../firebase/firebase';
import { withRouter } from 'react-router-dom';
import * as routes from '../../constants/routes';
import { FormControl, FormGroup, ControlLabel, Button } from 'react-bootstrap';

class MoreInfo extends Component {
    constructor(props) {
        super(props)
        this.onSubmit = this.onSubmit.bind(this);
        this.validateState = this.validateState.bind(this);
        this.renderFunc = this.renderFunc.bind(this);
        this.state = {
            name: '',
            empid: '',  
            errorflag: false,
            error: {},
        }
    }

    validateState() {
        let flag = false
        let error = {}
        this.setState({error: null})
        if(validate.isEmpty(this.state.name)){
            //console.log("no username")
            flag = true
            error.name = "Please enter a valid Name"
        }
        else {
            error.name = ""
        }
        if(validate.isEmpty(this.state.empid)){
            //console.log("no username")
            flag = true
            error.empid = "Please enter Employee ID"
        }
        else {
            error.empid = ""
        }
        
        if(flag) {
            this.setState({errorflag: flag,error: error})
        }
        
    }
    
    onSubmit(event) {
        this.validateState()

        const {
            history,
        } = this.props;
        
        if(this.state.errorflag === false){
            console.log(this.props.location.state.uid)
            userRef.child(this.props.location.state.uid).update({ name: this.state.name, empid: this.state.empid })
            .then(() => {
                //console.log(resp)
                history.push(routes.HOME);
            })
            .catch((error) => console.log(error));
        }
        event.preventDefault();
    }

    renderFunc() {
        
        if(this.state.errorflag && this.state.error !=null){
            if(this.state.error.name){
                return(
                    <p>{this.state.error.name}</p>
                )
            }
            if(this.state.error.empid){
                return(
                    <p>{this.state.error.empid}</p>
                )
            }
        }
        else {
            return (
                <p>hello</p>
            )
        }
    }

    render() {
        return (
            <div className='myFont bodyDiv row'>
                <div className='myAlign col-lg-1 col-centered'>


                    <form className='form'>
                        <FormGroup
                        controlId="formBasicText"
                        
                        //validationState={this.getValidationState()}
                        >
                            <ControlLabel className='myTitle'>More Info</ControlLabel>
                            <br/>
                            <br/>
                            <FormControl
                                type="text"
                                value={this.state.name}
                                placeholder="Enter name"
                                onChange={event => this.setState({name: event.target.value})}
                            />
                            <br/>
                            <FormControl
                                type="text"
                                value={this.state.empid}
                                placeholder="Enter Employee Id"
                                onChange={event => this.setState({empid: event.target.value})}
                            />
                            <br/>
                            <FormControl.Feedback />
                            <br/>
                            <Button type="submit" onClick={this.onSubmit}>Submit</Button>
                        </FormGroup>
                    </form>
                    {
                        this.renderFunc()
                    }
                </div>
                
            </div>
            
        )
    }
}

export default withRouter(MoreInfo);