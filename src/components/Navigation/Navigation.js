import React from 'react';
import AuthUserContext from '../AuthUserContext';
import NavigationAuth from '../NavigationAuth';
import NavigationNonAuth from '../NavigationNonAuth';
import NavigationAdminAuth from '../NavigationAdminAuth';

const Navigation = () =>
    <AuthUserContext.Consumer>
        {user => {
            if(user) {
                if(user.admin){
                    return(
                        <NavigationAdminAuth />
                    )
                }
                else { 
                    return(
                        <NavigationAuth />
                    )
                }
            }
            else {
                return (
                    <NavigationNonAuth />
                )
                
            }
        }}
    </AuthUserContext.Consumer>
export default Navigation;