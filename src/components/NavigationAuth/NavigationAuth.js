import React, { Component } from 'react';
import './NavigationAuth.css';
import { Link } from 'react-router-dom';
import * as routes from '../../constants/routes';
import Signout from '../Signout';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
class NavigationAuth extends Component {
    render() {
        return (
            <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                        <Link className='brandName' to={routes.LANDING}>pnch</Link>
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav pullRight>
                    <NavItem componentClass='span' eventKey={1} >
                        <Link to={routes.HISTORY}>History</Link>
                    </NavItem>
                    <NavItem componentClass='span' eventKey={2} >
                        <Link to={routes.HOME}>Home</Link>
                    </NavItem>
                    <NavItem componentClass='span' eventKey={3} >
                        <Signout/>
                    </NavItem>
                    
                </Nav>
            </Navbar>
        )
    }
}

export default NavigationAuth;