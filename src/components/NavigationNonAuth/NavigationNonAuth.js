import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as routes from '../../constants/routes';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import './NavigationNonAuth.css';
class NavigationNonAuth extends Component {
    render() {
        return (
            <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                        <Link className='brandName' to={routes.LANDING}>pnch</Link>
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav pullRight>
                    <NavItem componentClass='span' eventKey={1} >
                    <Link to={routes.SIGN_IN}>Sign In</Link>
                    </NavItem>
                    <NavItem componentClass='span' eventKey={2} >
                        <Link to={routes.SIGN_UP}>Sign Up</Link>
                    </NavItem>
                </Nav>
            </Navbar>
        )
    }
}

export default NavigationNonAuth;