import React, { Component } from 'react';

import { auth } from '../../firebase/firebase';
import * as routes from '../../constants/routes';
import { createBrowserHistory } from 'history'

const history = createBrowserHistory()

class Signout extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        this.doSignout = this.doSignout.bind(this);
    }

    doSignout() {
        auth.signOut()
        .then(() => {
            history.push({
                pathname: routes.LANDING
            })
        });
    }

    render() {
        return (
            <button
                type="button"
                onClick={this.doSignout}
            >
                Sign Out
            </button>
        )
    }
}
export default Signout;
