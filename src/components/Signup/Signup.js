import React, { Component } from 'react';
import './Signup.css';
import * as validate from '../../validate';
import { auth, userRef } from '../../firebase/firebase';
import { withRouter } from 'react-router-dom';
import * as routes from '../../constants/routes';
import { FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap'
class Signup extends Component {
    constructor(props) {
        super(props)
        this.onSubmit = this.onSubmit.bind(this);
        this.validateState = this.validateState.bind(this);
        this.renderFunc = this.renderFunc.bind(this);
        this.onSuccess = this.onSuccess.bind(this);
        this.onError = this.onError.bind(this);
        this.state = {
            username: '',
            email: '',
            pOne: '',
            pTwo: '',
            errorflag: false,
            error: {},
            ip: '',
        }
    }

    validateState() {
        let flag = false
        let error = {}
        this.setState({error: null})
        if(validate.isEmpty(this.state.username)){
            //console.log("no username")
            flag = true
            error.username = "Please enter a valid Username"
        }
        else {
            error.username = ""
        }
        if(!validate.validateEmail(this.state.email)){
            //console.log("Please enter a valid Email Id")
            flag = true
            error.email = "Please enter a valid Email Id"
        }
        else {
            error.email = ""
        }
        if(!validate.validatePassword(this.state.pOne, this.state.pTwo)){
            //console.log("Please enter a valid Email Id")
            flag = true
            //console.log(this.state.pOne)
            //console.log(this.state.pTwo)
            error.passwordlengthError = "Password has to be more than 6 charachter"
        }
        else {
            error.passwordlengthError = ""
        }
        if(!validate.confirmPassword(this.state.pOne, this.state.pTwo)){
            //console.log("Please enter a valid Email Id")
            flag = true
            error.passwordmismatchError = "Password Mismatch"
        }
        else {
            error.passwordmismatchError = ""
        }
        
        if(flag) {
            this.setState({errorflag: flag,error: error})
        }
        
    }

    onSuccess() {

    }

    onError() {
        console.log("error")
    }
    
    onSubmit(event) {
        this.validateState()
        const {
            history,
        } = this.props;
        if(this.state.errorflag === false){
            auth.createUserWithEmailAndPassword(this.state.email, this.state.pOne)
            .then((resp) => {
                console.log(resp.user.uid)
                
                userRef.child(resp.user.uid).update({ email: this.state.email, username: this.state.username })
                .then(() => {
                    //console.log(resp)
                    history.push({
                        pathname: routes.MOREINFO,
                        state: { uid: resp.user.uid }
                    })
                    
                })
                .catch((error) => console.log(error));
            })
            .catch((error) => console.log(error));
        }
        event.preventDefault();
    }

    renderFunc() {
        
        if(this.state.errorflag && this.state.error !=null){
            if(this.state.error.username){
                return(
                    <p>{this.state.error.username}</p>
                )
            }
            if(this.state.error.email){
                return(
                    <p>{this.state.error.email}</p>
                )
            }
            if(this.state.error.passwordlengthError){
                return(
                    <p>{this.state.error.passwordlengthError}</p>
                )
            }
            if(this.state.error.passwordmismatchError){
                return(
                    <p>{this.state.error.passwordmismatchError}</p>
                )
            }
        }
        else {
            return (
                <p>hello</p>
            )
        }
    }

    render() {
        return (
            <div className='myFont bodyDiv row'>
                <div className='myAlign col-lg-1 col-centered'>


                    <form className='form'>
                        <FormGroup
                        controlId="formBasicText"
                        
                        //validationState={this.getValidationState()}
                        >
                            <ControlLabel className='myTitle'>Sign up</ControlLabel>
                            <br/>
                            <br/>
                            <FormControl
                                type="text"
                                value={this.state.username}
                                placeholder="Enter username"
                                onChange={event => this.setState({username: event.target.value})}
                            />
                            <br/>
                            <FormControl
                                type="email"
                                value={this.state.email}
                                placeholder="Enter email"
                                onChange={event => this.setState({email: event.target.value})}
                            />
                            <br/>
                            <FormControl
                                type="password"
                                value={this.state.pOne}
                                placeholder="Enter password"
                                onChange={event => this.setState({pOne: event.target.value})}
                            />
                            <br/>
                            <FormControl
                                type="password"
                                value={this.state.pTwo}
                                placeholder="Confirm Password"
                                onChange={event => this.setState({pTwo: event.target.value})}
                            />
                            <FormControl.Feedback />
                            <br/>
                            <Button type="submit" onClick={this.onSubmit}>Submit</Button>
                        </FormGroup>
                    </form>
                    {
                        this.renderFunc()
                    }
                </div>
                
            </div>
        )
    }
}

export default withRouter(Signup);