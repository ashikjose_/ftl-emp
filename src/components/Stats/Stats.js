import React, { Component } from 'react';
import './Stats.css';
import AuthUserContext from '../AuthUserContext';
import BarChart from '../BarChart';
import EmpList from '../EmpList';
class Stats extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            arr: null, 
            chartReady: false,
            emp: null,
            empFlag: false,
            empSelect: null
        }
        this.renderFunc = this.renderFunc.bind(this);
        this.renderEmp = this.renderEmp.bind(this);
    }

    componentDidMount() {

    }

    componentWillMount() {


    }

    componentWillReceiveProps(props) {
        console.log("here it receives")
        let arr = props.user.analytic
        console.log(arr)
        let mainArr = [];
        for(var i in arr) {
            mainArr[i] = arr[i]
        }
        console.log(mainArr)
        this.setState({emp: mainArr, chartReady: true})
        console.log("End from Stats")
    }

    renderFunc() {
        
            /*this.state.emp.map((data) => {
                return(
                    //<Button className='buttonClass' bsSize='large' onClick={this.render(data)}>{data.name}</Button>
                )
            })*/
        
    }

    renderEmp(data) {
        console.log(data)
        this.setState({empFlag: true, empSelect: data})
    }

    render() {
        const {user} = this.props
        
        return(
            
            user ?
                <div className='row myWindow'>
                    <div className='col-lg-12 myFont '>       
                            <div>
                            <h4> Hello, {user.name}</h4>
                            {
                                this.state.chartReady ?
                                    this.state.emp.map((data) => {
                                        return (
                                            <EmpList className='buttonClass' data={data} funcCall={this.renderEmp}/>
                                        )
                                    })
                                    :
                                    <h2>Loading</h2>
                            }

                            {
                                this.state.empFlag ?
                                    <div>
                                        <h2>Stats for, {this.state.empSelect.user.data.name}</h2>
                                        <hr/>
                                        <h4>Monthly Stats</h4>
                                        
                                        <BarChart data={this.state.empSelect.user.monthly} size={[250,250]} />
                                        
                                        <hr/>
                                        <h4>Weekly Stats</h4>
                                        <BarChart data={this.state.empSelect.user.weekly} size={[250,250]} />
                                        <hr/>
                                        <hr/>
                                    </div>
                                :
                                    <h2>Select an Employee</h2>

                            }
                        </div>
                    </div>
                </div>
                
            :
            <div><h2>Loading</h2></div>
        
        )
    }

    
}

export default props => (
    <AuthUserContext.Consumer>
        {user=>{
            return (
                user ? <Stats {...props} user={user} />
                : <div/>
            )
        }}
    </AuthUserContext.Consumer>        
)
