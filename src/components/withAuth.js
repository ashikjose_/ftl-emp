import React from 'react';
import { userRef } from '../firebase/firebase';
import AuthUserContext from './AuthUserContext';
import { firebase } from '../firebase';
import moment from 'moment';


const withAuth = (Component) =>
  class WithAuth extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            authUser: null,
            user: null, 
            load: false
        };
        this.getName = this.getName.bind(this);
    }

    getName(uid) {
        let data = null
        userRef.child(uid).once('value').then((resp) => {
            
            data = { uid, ...resp.val()}
            if(data.admin){
                userRef.once('value').then((resp) => {
                    console.log(resp.val())
                    let arr = resp.val()
                    let analytic = [];
                    for(var i in arr){
                        if(!arr[i].admin){
                            let user = {};
                            user.data = arr[i];
                            user.hour = 0;
                            let month = [];
                            let week = [];
                            var init = 0;
                            for (init = 0 ;init<12;init++){
                                month[init] = 0
                            }

                            for (init = 0 ;init<moment().isoWeeksInYear();init++){
                                week[init] = 0
                            }
                            
                            if(arr[i].Sessions) {
                                let hist = arr[i].Sessions.History
                                let t = 0;
                                for(var j in hist) {
                                    let startDate = moment(hist[j].timestamp)
                                    let endDate = moment(hist[j].timestampEnd)
                                    let hours = endDate.diff(startDate)
                                    month[moment(hist[j].timestampEnd).month()]+=hours;
                                    week[moment(hist[j].timestampEnd).format("W")-1]+=hours;
                                    t = t + hours;
                                }
                                user.hour = t
                                user.weekly = week
                                user.monthly = month
                            }
                            analytic.push({user: user})
                        }
                    }
                    data = {...data, analytic}
                    this.setState({user: data, load: true})
                })
                .catch((error) => console.log(error))
            }
            else {
                this.setState({user: data, load: true})
            }
            
           
            
        })
        .catch((error) => console.log(error));
    }
    
    componentDidMount() {
        firebase.auth.onAuthStateChanged(authUser => {
            if(authUser) {
                this.setState({ authUser })
                this.getName(authUser.uid)

            } else {
                this.setState({ authUser: null });

            }
  
          });
    }
    
    componentWillMount() {
        firebase.auth.onAuthStateChanged(authUser => {
          if(authUser) {
            this.setState({ authUser })
            this.getName(authUser.uid)

          } else {
            this.setState({ authUser: null });

          }

        });
    }

    
    render() {
        if(this.state.load) {
            
                return (
                    <AuthUserContext.Provider value={this.state.user}>
                        <Component {...this.props} />
                    </AuthUserContext.Provider>
                )
            
        }
        else {
            return(
                <AuthUserContext.Provider value={null}>
                    <Component {...this.props} />
                </AuthUserContext.Provider>
            )
        }
        
    }
  }

export default withAuth;

