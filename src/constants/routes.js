export const SIGN_UP = '/signup';
export const SIGN_IN = '/signin';
export const LANDING = '/';
export const HOME = '/home';
export const HISTORY = '/history';
export const PASSWORD_FORGET = '/pw-forget';
export const MOREINFO = '/moreinfo';
export const STATS = '/stats';