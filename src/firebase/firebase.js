import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyBgnZraYikQjNBWjbMYZRI-4vb4Aas-x3U",
    authDomain: "employee-4f562.firebaseapp.com",
    databaseURL: "https://employee-4f562.firebaseio.com",
    projectId: "employee-4f562",
    storageBucket: "employee-4f562.appspot.com",
    messagingSenderId: "583920026994"
}

firebase.initializeApp(config);

export const auth = firebase.auth();
export const database = firebase.database();
export const userRef = database.ref().child('Users');

export const onceGetUsers = () =>
  database.ref('users').once('value');
  